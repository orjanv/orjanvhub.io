---
layout: post
title:  "Flere møter mellom gammelt og nytt i Stonehaven"
date:   2013-09-02 21:37:30
thumbnail: /assets/006-banner.jpg
category: historie
tags: historie gimp bildemanipulering foto historie stonehaven
---
I forrige innlegg skrev jeg om prosessen jeg fulgte når jeg blandet nytt og gammelt foto fra Stonehaven. Jeg hadde allerede planlagt å gjøre det med flere bilder fra samme sted. Jeg kom over et bilde fra Market Square i 1921, og du kan se nede til høyre to damer som er ute og spaserer. Det var tydelig en gågate og lite trafikk. 

![Mix]({{ site.url }}/assets/006a-mix.jpg)

Eneste trafikken var hest og vogn og sikkert ikke like mye som biler idag. Market Square er i dag en parkeringsplass, foruten noen lørdager og søndager hvor det brukes som markedsplass. En ting til er et digert tre som en gang stod på utkanten av markedsplassen, men som idag er vekk. 

Bildet under er fra Allardice Street, og du kan fortsatt se tårnet fra Market Square, som i bildet over. Dette er gaten på en måte hovedgaten i byen og du kan se hest og kjerrer i gaten. Til venstre kan du også se noen som spaserer med barnevogn og fine hatter. 

![Mix]({{ site.url }}/assets/006b-mix.jpg)

Slik som sist, brukte jeg Gimp for Ubuntu linux for å redigere bildene, men denne gangen brukte jeg 30 i feather edge istedet for 40, noe jeg syntes ga et bedre resultat. Jeg håper å finne flere gamle bilder som jeg kan blande med dagens Stonehaven. 

Jeg har ikke lyktes med å finne opphavsrett til disse gamle bildene og de jeg fant på internett hadde temmelig ukjent opphav og var i dårlig kvalitet.
