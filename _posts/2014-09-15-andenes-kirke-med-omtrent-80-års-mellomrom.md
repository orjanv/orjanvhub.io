---
layout: post
title:  "Andenes kirke med omtrent 80 års mellomrom"
date:   2014-09-15 21:37:30
thumbnail: /assets/002-banner.jpg
category: historie
tags: historie andenes gimp bildemanipulering foto historie
---
Under er et bilde av Andenes kirke fra en gang mellom 1925 og 1965:

![Gammel]({{ site.url }}/assets/002-old.jpg)

Andenes nye kirke ble vigslet 30.juni 1876 av biskop Hvoslef. Kirken var ikke bygd på "klippegrunn" og seig over til den ene siden, slik at den måtte støttes av på nordsiden en periode.

Slik ser kirka ut idag, med samme utsnitt, tatt fra blikkenslageren lengre ned:

![Ny]({{ site.url }}/assets/002-new.jpg)

Det gamle bildet er som sist, hentet fra www.andoy-historielag.org:
"Stagene ble satt opp etter en storm i 1925, og de ble revet ca. 1965. Finn Hansen, Andenes husker dette fordi han på den tida var med på å legge inn lys i kirka. Han har hørt at stagene skulle komme fra Dverberg. På en kjempestor stige stod årstallet 1843 risset inn i en av vangene. Han minnes også fra da han var barn at det var noen store stabbesteiner på sørsida av veien opp til kirka. De pleide ungene å bruke til å hoppe bukk."

![Mix]({{ site.url }}/assets/002-mix.jpg)

Når disse blir lagt på hverandre, kan vi tydelig se at bildet må være tatt fra omtrent samme sted og noen raske observasjoner:

* Treet var ikke plantet den gangen
* Tilbygget på venstre bakside fantes ikke den gangen
* En gammel pipe er fjernet fra taket
* Vinduene hadde sorte karmer før
* Det er senere gravd ut en kjelleringang til høyre bakbygg
