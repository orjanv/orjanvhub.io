---
layout: post
title:  "Forskere fra fortiden besøker gamle USOC på Andøya Space Center"
date:   2014-09-18 21:37:30
thumbnail: /assets/003-banner.jpg
category: historie
tags: historie andenes gimp bildemanipulering foto historie spacecenter
---
I en bok som heter "4 3 2 1 Fire", som omhandler historikken til Andøya Rakettskytefelt (idag Andøya Space Center), kom jeg over et bilde fra 70-tallet, hvor forskere står på verandaen til det gamle User Science Operation Centre (USOC), som idag er NAROM Telemetristasjon. 

![Gammel]({{ site.url }}/assets/003-old.jpg)

Idag ser denne verandaen litt annerledes ut og ifølge Tine Joramo på NAROM måtte de blant annet rive det lille uthuset med glassvindu for noen år siden.

![Ny]({{ site.url }}/assets/003-new.jpg)

Vi kan likevel bringe de gamle forskerne tilbake på verandaen en gang til ved å lappe disse sammen, kikke inn i et utvidet nøkkelhull inn i fortiden.

![Mix]({{ site.url }}/assets/003-mix.jpg)

Kanskje det er slik det er i Alex Scarrows "TimeRiders" serie, hvor de benyttet [tachyon partikler](http://en.wikipedia.org/wiki/Tachyon) til å reise i tid?
