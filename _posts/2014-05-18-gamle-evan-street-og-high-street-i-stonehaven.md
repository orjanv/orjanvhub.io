---
layout: post
title:  "Gamle Evan Street og High Street i Stonehaven"
date:   2014-05-18 21:37:30
thumbnail: /assets/004-banner.jpg
category: historie
tags: historie gimp bildemanipulering foto historie stonehaven
---
Her kommer tre nye sammensetninger av gamle og nye bilder fra Stonehaven. Denne gangen har jeg lånt en bok på det lokale biblioteket for å finne noen gode bilder å ta utgangspunkt i.

Det første bildet (1) viser en travel dag på Evan Street fra 1800-tallet, med torget til høyre slik det er i dag. Nå er torget parkeringsplass for mange biler på dagtid. Tidligere var det også et digert tre der, men borte idag. Til venstre på gatehjørnet, idag en optiker forretning, var fødestedet for oppfinneren Robert W. Thomson (f. 1822).

![Mix]({{ site.url }}/assets/004a-mix.jpg)

Robert Thomson, fant opp fyllepennen i 1857 og forandret skriving for alltid. Det ble mulig å skrive mer enn noen få ord uten å dyppe en penn med blekk, som fyllepenner inneholder sin egen forsyning av blekk (2).

Det andre bildet er High Street med Market Cross lengst bak. Fireball ceremony (4), en tradisjon som begynte i 1889 og foregår på nyttårsaften skjer i denne gaten. 

![Mix]({{ site.url }}/assets/004b-mix.jpg)

Tredje og siste bilde er som 1. bilde bare motsett vei. Her ser vi ned Evan Street mot torget på venstre side og havet i bakgrunnen. Bildet er fra 1930-tallet (5).

![Mix]({{ site.url }}/assets/004c-mix.jpg)

Kilder:

1. "[Old Stonehaven](http://www.amazon.co.uk/Old-Stonehaven-Brian-H-Watt/dp/1840330147)" av Brian H. Watt, side 20
2. [http://www.dobing.info/12/2013/06/fyllepenner-vs-kulepenner](http://www.dobing.info/12/2013/06/fyllepenner-vs-kulepenner.html).html
3. "[Old Stonehaven](http://www.amazon.co.uk/Old-Stonehaven-Brian-H-Watt/dp/1840330147)" av Brian H. Watt, side 6
4. [http://www.stonehavenfireballs.co.uk/about](http://www.stonehavenfireballs.co.uk/about)
5. "[Old Stonehaven](http://www.amazon.co.uk/Old-Stonehaven-Brian-H-Watt/dp/1840330147)" av Brian H. Watt, side 6
