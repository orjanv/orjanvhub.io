---
layout: post
title:  "Erna Borgens blomsterbutikk i Storgata"
date:   2014-09-13 21:37:30
thumbnail: /assets/001-banner.jpg
category: historie
tags: historie andenes gimp bildemanipulering foto historie
---
Jeg kom over et [nettsted](http://www.andoy-historielag.org) som har mange innsendte gamle bilder fra Andenes og fant raskt frem til noen få som jeg kunne jobbe med. 

![Gammel]({{ site.url }}/assets/001-old.jpg)

Et av bildene er tatt ikke så langt unna hvor jeg bor. Det er av Erna Borgens blomsterbutikk i Storgata. Bildet er sendt inn til Andøy Historiske av Rigmor Åse og lagt ut av Kolbjørn Blix Dahle. Det gamle bildet viser bussen som går til Risøyhamn fra Andenes idet den kjører forbi butikken. 

Hvis du ser nøye, kan du se sjåføren i bussen som bruker hatt. En annen detalj er at inngangspartiet i butikken er flyttet til sørsiden og ikke lengre vestsiden som vender mot Storgata. 

![Gammel]({{ site.url }}/assets/001-new.jpg)

Når jeg legger disse oppå hverandre, blir resultatet som følger. Litt vanskelig ettersom bildeformat og kamera brukt den gang er helt annerledes enn kameraer idag. 

![Gammel]({{ site.url }}/assets/001-mix.jpg)

Det var en del bilder på Andøy Historiske, men som jeg forstår er en del ikke lengre å finne på grunn av gamle branner. Hvis du har et forslag til et godt bilde som jeg kan bruke, er du velkommen til å tipse meg om det.
