---
layout: post
title:  "På nært hold - bakgrunnsbilder av gamle ting"
date:   2016-02-14 09:02:54 
thumbnail: /assets/wallpaper-2016-007.jpg
category: foto
tags: lukt gammel retro foto makro wallpaper bakgrunnsbilder
---

Jeg er glad i å ta bilder av en rekke ting, men å få det en normalt ikke ser er ekstra spesielt.

Her følger en rekke bilder jeg har tatt på nært hold av gamle fine ting. De er alle i 16:9 størrelsesforhold slik at du kan bruke de som bakgrunnsbilder på telefon og PC. 

Høyreklikk og velg lagre som.. for å laste ned bildene. Ønsker du alle sammen, kan du laste ned denne [zip-filen](/assets/makro-wallpaper-2016-02.zip).

**Playmobilmann med skjegg**

![](/assets/wallpaper-2016-001.jpg "Playmobilmann med skjegg")

**Gammel nøkkel med snirkler**

![](/assets/wallpaper-2016-002.jpg "Gammel nøkkel med snirkler")

**Samme gamle nøkkel i annen vinkel**

![](/assets/wallpaper-2016-003.jpg "Samme gamle nøkkel i annen vinkel")

**Gammel bok med en samling gudstjenester fra britisk kirke**

![](/assets/wallpaper-2016-004.jpg "Gammel bok med en samling gudstjenester fra britisk kirke")

**Fra en side i samme bok**

![](/assets/wallpaper-2016-005.jpg "Fra en side i samme bok")

**Fra permen på samme bok**

![](/assets/wallpaper-2016-006.jpg "Fra permen på samme bok")

**Arkene på boken**

![](/assets/wallpaper-2016-007.jpg "Arkene på boken")

**Ark og perm på boken**

![](/assets/wallpaper-2016-008.jpg "Ark og perm på boken")

**Fronten av en gammel hotwheelsbil jeg hadde som liten**

![](/assets/wallpaper-2016-009.jpg "Fronten av en gammel hotwheelsbil jeg hadde som liten")

**Bakenden av en gammel hotwheelsbil jeg hadde som liten**

![](/assets/wallpaper-2016-010.jpg "Bakenden av en gammel hotwheelsbil jeg hadde som liten")

