---
layout: post
title: "Noen bilder fra Dverberg på Andøy"
date: 2016-04-17 09:49:18 +0200
thumbnail: /assets/DSC_3769.jpg
category: foto
tags: foto bilde dverberg andøy
---

Her er noen bilder fra rundt omkring på Dverberg, et lite tettsted på Andøy i Nordland fylke. 

![]({{ site.url }}/assets/DSC_3728.jpg)

![]({{ site.url }}/assets/DSC_3742.jpg)

![]({{ site.url }}/assets/DSC_3747.jpg)

![]({{ site.url }}/assets/DSC_3757.jpg)

![]({{ site.url }}/assets/DSC_3769.jpg)

![]({{ site.url }}/assets/DSC_3772.jpg)

![]({{ site.url }}/assets/IMG_20160416_144841.jpg)

[Se bildene i høyere oppløsning på flickr](https://www.flickr.com/photos/hoyd/albums/72157667240309295/with/26450035356/)

