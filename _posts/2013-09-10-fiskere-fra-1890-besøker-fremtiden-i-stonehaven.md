---
layout: post
title:  "Fiskere fra 1890 besøker framtiden i Stonehaven"
date:   2013-09-10 21:37:30
thumbnail: /assets/005-banner.jpg
category: historie
tags: historie gimp bildemanipulering foto historie stonehaven
---
Her kommer enda et bilde med gammelt og nytt lagt oppå hverandre. Det gamle bildet er fra 1890 og det nye fra idag. Begge er fra havna i Stonehaven. Byggene i bakgrunnen har forandret seg noe, så det var ikke helt enkelt å plassere det gamle bildet. 

![Mix]({{ site.url }}/assets/005-mix.jpg)
