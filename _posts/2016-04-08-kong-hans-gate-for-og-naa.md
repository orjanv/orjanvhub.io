---
layout: post
title: "Kong Hans Gate på 50-tallet og idag"
date: 2016-04-08 13:39:06 
thumbnail: /assets/kong_hans_gate_sett_sorover_fra_nummer_12_-_50-tallet.jpg
category: foto
tags: gimp foto bilde historie andenes andøy
---

Klikk på bildet for å bytte mellom 50-tallet og idag.

<img id="myImage" onclick="fade(changeImage())" src="{{ site.url }}/assets/kong_hans_gate_sett_sorover_fra_nummer_12_-_50-tallet.jpg" width="100%">

<script>
function fade(el){
	var elem = document.getElementById(el);
	elem.style.transition = "opacity 0.5s linear 0s";
	elem.style.opacity = 1;
}
function changeImage() {
    var image = document.getElementById('myImage');
    if (image.src.match("2016")) {
        image.src = "{{ site.url }}/assets/kong_hans_gate_sett_sorover_fra_nummer_12_-_50-tallet.jpg";
    } else {
        image.src = "{{ site.url }}/assets/kong_hans_gate_sett_sorover_fra_nummer_12_-_2016.jpg";
    }
}
</script>

***

Kilder: 

* *[Det var en gang på Andenes Facebook gruppe](https://www.facebook.com/groups/395943463908207/permalink/623793334456551/)*

