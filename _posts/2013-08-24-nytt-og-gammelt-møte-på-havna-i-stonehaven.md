---
layout: post
title:  "Nytt og gammelt møte på havna i Stonehaven"
date:   2013-08-24 21:37:30
thumbnail: /assets/007-banner.jpg
category: historie
tags: historie gimp bildemanipulering foto historie stonehaven
---
Kom over en bildeserie av gamle bilder satt i nye bilder og likte effekten det ga. Her prøver jeg meg med det samme, fra havnen i Stonehaven.

![Mix]({{ site.url }}/assets/007-mix.jpg)

Det nye tok jeg i dag og det gamle bildet er fra 1900-tallet en gang. Det er en fisker og bildet har stått i Evening Express, 16 May 2004. 

Alene ser bildene slik ut:

![Ny]({{ site.url }}/assets/007-ny.jpg)
 
![Gammel]({{ site.url }}/assets/007-gammel.jpg)

Jeg har brukt Gimp med Free Select Tool og Feather Edger = 25 for å unngå skarpe overganger mellom bildene.
