---
layout: post
title: "Påsken - hva handler det egentlig om?"
date: 2016-03-15 20:28:15 
thumbnail: /assets/paasken.jpg
category: kristendom
tags: tro kristendom påske religion
---

Påsken handler i bunn og grunn om to ting:

1. Jesus dør
2. Jesus står opp igjen

Først, årsaken til at Jesus måtte dø var for alle vi andre mennesker hadde kommet bort fra Gud. Gud ble på en måte usynlig for oss og vi glemte av han. Gud ville derimot at vi skulle komme tilbake til han og sendte Jesus for å rydde opp i dette. 

Så, om Jesus forble død, ville det ikke vært noe særlig til hjelp for oss. Jesus måtte derfor bli levende igjen for å vise oss at døden ikke var slutten på alt. Dette var forutsagt i det gamle testamentet, men ingen på den tiden skjønte det. De fikk det oppklart når Jesus forklarte det til dem etter at han var blitt levende igjen. Samtidig viser han oss at døden ikke er slutten for oss andre mennesker heller.

Gjennom å tro på Jesus vil han passe på oss med sine engler mens vi lever og etter at vi er blitt gamle og dør, skal vi også bli levende igjen. Det er det håpet vi har gjennom å tro på Jesus. 

> Derfor er påsken viktig, fordi den minner oss på at alt står og faller på om Jesus faktisk ble levende igjen. Likt et frø som blir sådd i jorden oppstår det en ny plante. 

Om Jesus ikke ble levende igjen er alt vi tror på bortkastet og uten mening, hele kristendommen er lik null. Men om han ble levende igjen, har vi et enormt håp som  påvirker alt  vi gjør mens vi lever. 

![Den Første Påsken - NRK Super](https://gfx.nrk.no/kCe-0m8FbmWJq_S8S3pVTgbd5DG50Q4c4rgRqhSN67Ew)

***

Kilder: 

* *[Oppstandelsen, 1. Kor 15 - bibel.no](http://www.bibel.no/Nettbibelen.aspx?parse=1%20Kor%2015)*
* *[NRK - Den første påsken](https://tv.nrk.no/program/fbui5000280Q0/den-foerste-paasken)*
