---
layout: post
title:  "Lukten av gamle bøker"
date:   2015-08-03 21:37:30
thumbnail: /assets/gamle-boker.jpg
category: historie
tags: historie lukt sanser filosofi
---

Jeg elsker å lukte i gamle bøker. En gang for flere år siden bestilte jeg en gammel brukt bok av [C. S. Lewis](http://www.goodreads.com/author/show/1069006.C_S_Lewis) fra en [antikvariat](http://antikvariat.net/), og når jeg fikk boken var det tydelig at den hadde stått i en hylle i lengre tid. Selve boken var nok enda noen år eldre og sidene var begynt å gulne. Når jeg åpnet boken, strømmer det ut en lukt av historie, en lukt som får meg til å tenke på tiden og at den er gammel. 

![](/assets/gamle-boker.jpg) 

Noen har [tidligere stilt spørsmålet](http://english.stackexchange.com/questions/57416/word-for-the-smell-of-old-books) om det finnes et ord for denne lukten av gamle bøker:

> *Is there any special word for the smell of old books? I know about the use of musty to describe them. But I thought there could be a special word too, just like petrichor for the smell of fresh earth following rain.*

*Petrichor* er altså ordet for lukten av regn. Det står litt om dette på [Store Norske](https://snl.no/lukten_etter_regn) leksikon på nett. Som spørsmålet over, burde det kanskje finnes et tilsvarende ord for lukten av gamle bøker, gamle møbler, gammelt treverk eller gamle rom? Har kanskje historie i seg selv en egen lukt? Er historie egentlig noe du kan ta å føle på? Vi kan ta på gamle kirker og eldgamle trær og lukter ikke gammelt forskjellige fra nytt? 

Min [arbeidsplass](http://www.andoyaspace.no) har en eldre del og en ny del. Den gamle delen ble påbegynt på 60-tallet og har blitt bygd ut siden da, mens den nye delen er et par-tre år gammel. Jeg holder til i den nye delen, men av og til er det behov for å gjøre noe i den gamle delen, noen gang fordi jeg må gjøre noe der, andre ganger rett og slett bare for å lukte på historien må jeg innrømme. Og da mener jeg at treverket og rommene lukter annerledes, de har en god gammel lukt i seg. Det kan hende at det jeg liker med lukten er en assosiasjon til noe jeg forbinder med lukten. Jeg har en mistanke om hva det kan være, men jeg tror ikke det er bare det. Usikkert, men jeg liker det i alle fall. 

Fikk jeg valget, ville jeg nok valgt å ha kontor i den gamle delen. Hva om jeg hadde det, ville da den gamle delen som lukter av historie ikke lengre være spesiell etter en tid? Mulig det.