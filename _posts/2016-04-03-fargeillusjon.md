---
layout: post
title: "Bevis at felt A og B har samme farge"
date: 2016-04-03 10:44:00 
thumbnail: /assets/illusion_cover.png
category: illusjon
tags: illusjon gimp bilde
---

Husker du denne optiske illusjonen?

![Checker shadow illusion]({{ site.url }}/assets/Grey_square_optical_illusion.png)

Ved å bruke Gimp, har jeg her bevist at felt A og B har nøyaktig samme farge:

<iframe width="640" height="360" src="https://www.youtube.com/embed/eDnLZa7RAWE" frameborder="0" allowfullscreen></iframe>

***

Kilder: 

* *[Checker shadow illusion](https://en.wikipedia.org/wiki/Checker_shadow_illusion)*
* *[Optical illusion proved - Square A and B are the same color](https://www.youtube.com/watch?v=eDnLZa7RAWE)*

