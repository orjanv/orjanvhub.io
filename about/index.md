---
layout: page
title: About
permalink: /about/
---

![Me]({{ site.url }}/assets/me.jpg)

I'm curious and like to know how things, life and the world around me work. 

I live at Andenes, Andøya in Northern Norway, working as an Electronics Engineer and Teacher at [NAROM (Norwegian Centre for Space-related Education)](https://www.narom.no). NAROM initiates, develops, and performs educational activities, seminars and conferences at all levels of education within subject areas related to space, such as space technology, space physics, atmosphere and environment.

NAROM is a part of [Andøya Space Center (ASC)](http://www.andoyaspace.no) who supports sounding rocket and balloon operations both at Andøya and at Svalbard, and is host to a large array of ground based scientific instruments. ASC also owns and operates the ALOMAR lidar observatory located at the top of the nearby mountain – Ramnan (380 m above sea level). Our clients include ESA, NASA, JAXA as well as national and international universities and institutes. The range employs electronic, explosive, and safety experts among other specialists and an administration. ASC is a limited company owned 90% by the Department of Trade and Industry, and 10% by Kongsberg Defence Systems.

My contact information can be found below.
